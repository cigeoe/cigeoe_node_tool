CIGeoE Node Tool

Tool to perform operations over nodes of a selected feature, not provided by similar tools and plugins.

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_node_tool” to folder:

  ```bash
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Node Tool”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_node_tool” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip”, choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - After the installation the tool icon became available

2 - It is only possible to use the plugin when the layer is in editing mode

3 - When active, a window with a table opens. This is filled with information about the X, Y and Z of the nodes of each selected feature. When the mouse is over one of the nodes, it is selected in the table

4 - Creating a new node, either by double clicking on an edge or by clicking and dragging its center, a new node will be created. Your Z is calculated by interpolating the 2 adjacent nodes. Uses IDW (the distances used in interpolation are calculated between the coordinates of the new point and those of the 2 adjacent points)

5 - Result:

![ALT](/images/image1.png)

![ALT](/images/image2.png)

# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
