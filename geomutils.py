#***************************************************************************
#    CIGeoENodeTool adapted from following work (JUL17): 
#                       CadNodeTool
#-----------------------------------------------------------
# Copyright (C) 2015 Martin Dobias
#-----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
#***************************************************************************
# This program is free software; you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation; either version 2 of the License, or     
# (at your option) any later version.                                   
#-----------------------------------------------------------

from qgis.core import *


def is_endpoint_at_vertex_index(geom, vertex_index):                                        # geom = QgsLineString
    """ Find out whether vertex at the given index is an endpoint (assuming linear geometry) """
            
    #g = geom.geometry()          # qgis2  'QgsGeometry' object has no attribute 'geometry' in qgis3    
    g = geom.constGet()           # qgis3
    
    if isinstance(g, QgsCurve):
        return vertex_index == 0 or vertex_index == g.numPoints()-1
    elif isinstance(g, QgsMultiCurve): 
        #for i in xrange(g.numGeometries()):        # qgis2
        for i in range(g.numGeometries()):          # qgis3
            part = g.geometryN(i)
            if vertex_index < part.numPoints():
                return vertex_index == 0 or vertex_index == part.numPoints()-1
            vertex_index -= part.numPoints()    
    else:
        assert False                    
    

def vertex_at_vertex_index(geom, vertex_index):
    """ Get coordinates of the vertex at particular index """
    #g = geom.geometry()                            # qgis2
    g = geom.constGet()                             # qgis3
    #p = QgsPointV2()                               # qgis2
    p = QgsPoint()                                  # qgis3
    if isinstance(g, QgsCurve):
        g.pointAt(vertex_index, p)
    elif isinstance(g, QgsMultiCurve):
        #for i in xrange(g.numGeometries()):        # qgis2
        for i in range(g.numGeometries()):          # qgis3
            part = g.geometryN(i)
            if vertex_index < part.numPoints():
                part.pointAt(vertex_index, p)
                break
            vertex_index -= part.numPoints()
    else:
        assert False
    return QgsPointXY(p.x(), p.y())


def adjacent_vertex_index_to_endpoint(geom, vertex_index):
    """ Return index of vertex adjacent to the given endpoint. Assuming linear geometries. """
    #g = geom.geometry()                            # qgis2
    g = geom.constGet()                             # qgis3
    if isinstance(g, QgsCurve):
        return 1 if vertex_index == 0 else g.numPoints()-2
    elif isinstance(g, QgsMultiCurve):
        offset = 0
        #for i in xrange(g.numGeometries()):            # qgis2
        for i in range(g.numGeometries()):              # qgis3
            part = g.geometryN(i)
            if vertex_index < part.numPoints():
                return offset+1 if vertex_index == 0 else offset+part.numPoints()-2
            vertex_index -= part.numPoints()
            offset += part.numPoints()
    else:
        assert False


def vertex_index_to_tuple(g, vertex_index):
    """ Return a tuple (part, vertex) from vertex index """

    if isinstance(g, QgsGeometry):
        #g = g.geometry()                               # qgis2
        g = g.constGet()                                # qgis3

    if isinstance(g, QgsGeometryCollection):
        part_index = 0
        offset = 0
        #for i in xrange(g.numGeometries()):            # qgis2
        for i in range(g.numGeometries()):              # qgis3
            part = g.geometryN(i)
            if vertex_index < part.numPoints():
                (_,ring_index,vertex) = vertex_index_to_tuple(part, vertex_index)
                return (part_index, ring_index, vertex)
            vertex_index -= part.numPoints()
            offset += part.numPoints()
            part_index += 1

    elif isinstance(g, QgsCurve):
        return (0, 0, vertex_index)

    elif isinstance(g, QgsCurvePolygon):
        ring = g.exteriorRing()
        if vertex_index < ring.numPoints():
            return (0, 0, vertex_index)
        vertex_index -= ring.numPoints()
        ring_index = 1
        #for i in xrange(g.numInteriorRings()):             # qgis2
        for i in range(g.numInteriorRings()):               # qgis3
            ring = g.interiorRing(i)
            if vertex_index < ring.numPoints():
                return (0, ring_index, vertex_index)
            vertex_index -= ring.numPoints()
            ring_index += 1


if True:  # testing
    line = QgsGeometry.fromWkt("LINESTRING(1 1, 2 1, 3 2)")
    assert is_endpoint_at_vertex_index(line, 0) == True
    assert is_endpoint_at_vertex_index(line, 1) == False
    assert is_endpoint_at_vertex_index(line, 2) == True
    assert vertex_at_vertex_index(line, 0) == QgsPointXY(1, 1)
    assert vertex_at_vertex_index(line, 1) == QgsPointXY(2, 1)
    assert vertex_at_vertex_index(line, 2) == QgsPointXY(3, 2)
    assert adjacent_vertex_index_to_endpoint(line, 0) == 1
    assert adjacent_vertex_index_to_endpoint(line, 2) == 1
    assert vertex_index_to_tuple(line, 0) == (0, 0, 0)
    assert vertex_index_to_tuple(line, 2) == (0, 0, 2)

    mline = QgsGeometry.fromWkt("MULTILINESTRING((1 1, 2 1, 3 2), (3 3, 4 3, 4 2))")
    assert is_endpoint_at_vertex_index(mline, 0) == True
    assert is_endpoint_at_vertex_index(mline, 1) == False
    assert is_endpoint_at_vertex_index(mline, 2) == True
    assert is_endpoint_at_vertex_index(mline, 3) == True
    assert is_endpoint_at_vertex_index(mline, 4) == False
    assert is_endpoint_at_vertex_index(mline, 5) == True
    assert vertex_at_vertex_index(mline, 0) == QgsPointXY(1, 1)
    assert vertex_at_vertex_index(mline, 1) == QgsPointXY(2, 1)
    assert vertex_at_vertex_index(mline, 2) == QgsPointXY(3, 2)
    assert vertex_at_vertex_index(mline, 3) == QgsPointXY(3, 3)
    assert vertex_at_vertex_index(mline, 4) == QgsPointXY(4, 3)
    assert vertex_at_vertex_index(mline, 5) == QgsPointXY(4, 2)
    assert adjacent_vertex_index_to_endpoint(mline, 0) == 1
    assert adjacent_vertex_index_to_endpoint(mline, 2) == 1
    assert adjacent_vertex_index_to_endpoint(mline, 3) == 4
    assert adjacent_vertex_index_to_endpoint(mline, 5) == 4
    assert vertex_index_to_tuple(mline, 0) == (0, 0, 0)
    assert vertex_index_to_tuple(mline, 2) == (0, 0, 2)
    assert vertex_index_to_tuple(mline, 3) == (1, 0, 0)
    assert vertex_index_to_tuple(mline, 5) == (1, 0, 2)
