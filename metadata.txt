# This file contains metadata for your plugin.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=CIGeoE Node Tool
qgisMinimumVersion=3.0
description=Tool to perform operations over nodes of a selected feature, not provided by similar tools and plugins.
version=1.0
author=Centro de Informação Geoespacial do Exército 
email=igeoe@igeoe.pt

about=This plugin was adapted from https://github.com/wonder-sk/CadNodeTool, from Martin Dobias, extending some capabilities for 3D. For instance, when adding new nodes, it sets it's Z has an interpolated value from adjacent nodes. Also, it shows a vertex coordinates table, like QGis standard Node Tool.

tracker=https://gitlab.com/cigeoe/cigeoe_node_tool/-/issues
repository=https://gitlab.com/cigeoe/cigeoe_node_tool
# End of mandatory metadata

# Recommended items:

hasProcessingProvider=no
# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=vector,layers,nodes

homepage=https://gitlab.com/cigeoe/cigeoe_node_tool
category=Plugins
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Since QGIS 3.8, a comma separated list of plugins to be installed
# (or upgraded) can be specified.
# Check the documentation for more information.
# plugin_dependencies=

Category of the plugin: Raster, Vector, Database or Web
# category=

# If the plugin can run on QGIS Server.
server=False

