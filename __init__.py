# -*- coding: utf-8 -*-
#***************************************************************************
#    CIGeoENodeTool adapted from following work (JUL17): 
#                       CadNodeTool
#-----------------------------------------------------------
# Copyright (C) 2015 Martin Dobias
#-----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
#***************************************************************************
# This program is free software; you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation; either version 2 of the License, or     
# (at your option) any later version.                                   
#-----------------------------------------------------------


from qgis.PyQt.QtCore import *  # QSettings, QTranslator, QCoreApplication, Qt
from qgis.PyQt.QtGui import *  # QIcon, QColor
from qgis.PyQt.QtWidgets import * #QAction, QToolBar, QMessageBox, QTableWidget, QTableView
from qgis.core import *
from qgis.gui import *

from .cigeoe_node_tool_dockwidget import CIGeoENodeToolDockWidget
from .cigeoenodetool import CIGeoENodeTool
from .cigeoenodetool import MyTableWidget
from .resources import *


# CUSTOM START
# Import the code for the DockWidget
# CUSTOM END
def classFactory(iface):  
    return CIGeoENodeToolPlugin(iface)


class CIGeoENodeToolPlugin:

    def __init__(self, iface):
        self.iface = iface
        self.current_layer = None

        #CUSTOM START
        self.pluginIsActive = False
        self.dockwidget = None
        #CUSTOM END

    def initGui(self):
        icon_path = ':/plugins/cigeoe_node_tool/icon.png'
        icon = QIcon(icon_path)
        self.action = QAction(icon, "CIGeoE Node Tool", self.iface.mainWindow())
        self.action.setCheckable(True)
        self.action.triggered.connect(self.run)
        #CUSTOM START

        cigeoeToolBarExists = False
        for x in self.iface.mainWindow().findChildren(QToolBar): 
            if x.windowTitle() == 'CIGeoE':
                self.toolbar = x
                cigeoeToolBarExists = True
        if cigeoeToolBarExists==False:
            self.toolbar = self.iface.addToolBar(u'CIGeoE')


        self.toolbar.setObjectName(u'CIGeoE Node Tool')
        self.toolbar.addAction(self.action)

        self.iface.addPluginToMenu(
            u'&CIGeoE: Node Tool',
            self.action)


        #CUSTOM END

        self.iface.currentLayerChanged.connect(self.onCurrentLayerChanged)
        self.tool = CIGeoENodeTool(self.iface, self.iface.mapCanvas(), self.iface.cadDockWidget(), self)
        self.tool.setAction(self.action)


    def unload(self):        
        self.iface.removeToolBarIcon(self.action)
        self.iface.mapCanvas().unsetMapTool(self.tool)
        del self.action
        del self.tool
        
        self.deactivate()


    # CUSTOM START
    def deactivate(self):        
        if self.dockwidget is not None:
            self.dockwidget.closingPlugin.disconnect(self.deactivate)
            self.iface.removeDockWidget(self.dockwidget)
            self.dockwidget.t.selectionModel().selectionChanged.disconnect(self.tool.tableSelectionChanged)
            
        self.pluginIsActive = False
        self.dockwidget = None
        
    # CUSTOM END   

    def run(self):
        self.iface.mapCanvas().setMapTool(self.tool)

        # CUSTOM START
        if not self.pluginIsActive:
            self.pluginIsActive = True
            if self.dockwidget == None:
                self.dockwidget = CIGeoENodeToolDockWidget()


                #OVERRIDE TABLE WIDGET
                mytable=MyTableWidget(self.tool)
                self.dockwidget.setWidget(mytable)
                self.dockwidget.t=mytable
                self.dockwidget.t.setSelectionMode(QTableView.SingleSelection);
                self.dockwidget.t.setSelectionBehavior(QTableView.SelectRows);
                self.dockwidget.t.selectionModel().selectionChanged.connect(self.tool.tableSelectionChanged)



            # connect to provide cleanup on closing of dockwidget
            self.dockwidget.closingPlugin.connect(self.deactivate)
            # show the dockwidget
            self.iface.addDockWidget(Qt.LeftDockWidgetArea, self.dockwidget)
            self.dockwidget.show()
        # CUSTOM END

    
    def onCurrentLayerChanged(self):
        if isinstance(self.current_layer, QgsVectorLayer):  
            try:          
                self.current_layer.editingStarted.disconnect(self.onEditingStartStop)
                self.current_layer.editingStopped.disconnect(self.onEditingStartStop)            
            except RuntimeError as err:
                QgsMessageLog.logMessage("RuntimeError in disconnect object : "+ str( err )+")", 'CIGeoE Node Tool')

        self.action.setEnabled(self.tool.can_use_current_layer())        
        self.current_layer = self.iface.mapCanvas().currentLayer()
        if isinstance(self.current_layer, QgsVectorLayer):
            self.current_layer.editingStarted.connect(self.onEditingStartStop)
            self.current_layer.editingStopped.connect(self.onEditingStartStop)
    

    def onEditingStartStop(self):        
        self.action.setEnabled(self.tool.can_use_current_layer())       
         
        